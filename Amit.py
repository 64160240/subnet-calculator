import ipaddress


network_class = input("Network Class (A, B, or C): ")
ip_address = input("IP Address: ")
subnet_mask = input("Subnet Mask: ")
subnet_bits = int(input("Subnet Bits: "))
mask_bits = int(input("Mask Bits: "))
max_subnets = int(input("Maximum Subnets: "))
hosts_per_subnet = int(input("Hosts per Subnet: "))


network = ipaddress.IPv4Network(f"{ip_address}/{subnet_mask}", strict=False)
network_address = str(network.network_address)


broadcast_address = str(network.broadcast_address)

subnet_id = network.network_address

first_ip = str(network.network_address + 1)
last_ip = str(network.broadcast_address - 1)

subnet_info = {
    "Network Class": network_class,
    "IP Address": ip_address,
    "Subnet Mask": subnet_mask,
    "Subnet Bits": subnet_bits,
    "Mask Bits": mask_bits,
    "Maximum Subnets": max_subnets,
    "Hosts per Subnet": hosts_per_subnet,
    "Host Address Range": f"{first_ip} - {last_ip}",
    "Subnet ID": subnet_id,
    "Broadcast Address": broadcast_address,
    "Subnet Bitmap": f"10{'n'*(32-subnet_bits)}{'0'*subnet_bits}{'s'*(32-mask_bits)}{'h'*(32- mask_bits)}" 
}


print("Result")
for key, value in subnet_info.items():
    print(f"{key}\n{value}")
